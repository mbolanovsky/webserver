package main

import "sync"

type ErrorResponse struct {
	Error string `json:"error"`
}

type Author struct {
	Name      string `json:"name"`
	AuthorKey string `json:"authorKey"`
}

type AuthorKeys struct {
	Authors []AuthorKey `json:"authors"`
}

type AuthorKey struct {
	Key string `json:"key"`
}

type EndpointWorksResponse struct {
	Name        string `json:"name"`
	Revision    int    `json:"revision"`
	PublishDate string `json:"publishDate"`
}

type Work struct {
	Title    string `json:"title"`
	Revision int    `json:"revision"`
	Created  struct {
		Value string `json:"value"`
	}
}

type WorksResponse struct {
	Entries []Work `json:"entries"`
}

type CensoredDb struct {
	mux             sync.RWMutex
	censoredAuthors map[string]string
}
