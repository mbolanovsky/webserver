package main

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var openLibraryUrl string
var censoredDb *CensoredDb

func main() {

	// @BasePath /api/v1
	apiUrl := "api/v1"
	openLibraryUrl = "https://openlibrary.org"
	//censoredDb = censoredDb.GetInstace(censoredDb)
	//censoredDb.saveCensoredAuthors(config.Censors)
	r := gin.Default()
	//r.Use(handler())

	r.GET("/", getHello)
	//r.GET(apiUrl+"/version", enpGetVersion)
	r.GET(apiUrl+"/authors", enpGetAuthorsByBookId)
	r.GET(apiUrl+"/books", enpGetBooksByAuthor)
	r.POST(apiUrl+"/censors", enpSaveAuthorsToCensors)

	r.Run(":8080")
	// http.ListenAndServe(":2112", nil)

	/*server := http.Server{
		Addr:    ":8080",
		Handler: r,
	}*/
	// http://localhost:8080/api/v1/authors?book=9780140328721
	// http://localhost:8080/api/v1/books?author=OL23919A
	// curl -X PUT localhost:8080/api/v1/censors -d '["OL225086A","OL23919A"]'
}

func getHello(c *gin.Context) {
	c.String(http.StatusOK, "Hello %s %s", "Hello", "from gin")
}

func enpGetAuthorsByBookId(c *gin.Context) {
	bookId := c.Query("book")
	if bookId == "" {
		c.Writer.WriteHeader(http.StatusBadRequest)
		return
	}

	var authorKeys AuthorKeys
	var authorArr []Author

	err := getAuthorsKey(bookId, &authorKeys)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
		return
	}

	for _, item := range authorKeys.Authors {
		var author Author
		getAuthorByKey(item.Key, &author)
		authorArr = append(authorArr, Author{author.Name, strings.Trim(item.Key, "/authors/")})
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
		return
	}

	c.JSON(200, authorArr)
}

func getAuthorsKey(bookId string, authorKeys *AuthorKeys) error {
	res, err := http.Get(openLibraryUrl + "/isbn/" + bookId + ".json")
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)
	json.Unmarshal(body, authorKeys)
	return nil
}

func getAuthorByKey(key string, authors *Author) error {
	res, err := http.Get(openLibraryUrl + key + ".json")
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)
	json.Unmarshal(body, authors)
	return nil

}

func enpGetBooksByAuthor(c *gin.Context) {

	authorId := c.Query("author")
	if authorId == "" {
		// c.Writer.WriteHeader(http.StatusBadRequest)
		c.Status(http.StatusBadRequest)
		return
	}

	works, err := getAuthorsWorks(authorId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, ErrorResponse{Error: err.Error()})
		return
	}

	var worksRes []EndpointWorksResponse
	for _, item := range works {
		worksRes = append(worksRes, EndpointWorksResponse{Name: item.Title, Revision: item.Revision, PublishDate: item.Created.Value})

	}

	c.JSON(http.StatusOK, worksRes)

}

func getAuthorsWorks(authorOl string) ([]Work, error) {

	if censoredDb.checkCensoredAuthors(authorOl) {
		return nil, errors.New("Author censored")
	}

	resp, err := http.Get(openLibraryUrl + "/authors/" + authorOl + "/works.json")
	if err != nil {
		return []Work{}, err
	}

	decoder := json.NewDecoder(resp.Body)

	entries := WorksResponse{}
	err = decoder.Decode(&entries)
	return entries.Entries, err
}

func enpSaveAuthorsToCensors(c *gin.Context) {
	authors := make([]string, 0)

	c.BindJSON(&authors)

	censoredDb = censoredDb.GetInstace(censoredDb)
	censoredDb.saveCensoredAuthors(authors)
	censoredDb.getCensoredAuthors()
	//viper.Set("censors", censoredDb.getCensoredAuthors())
	//viper.WriteConfig()
	//
	c.Writer.WriteHeader(http.StatusOK)
}

func (cdb *CensoredDb) GetInstace(instance *CensoredDb) *CensoredDb {
	if instance == nil {
		return &CensoredDb{censoredAuthors: make(map[string]string, 0)}
	}
	return instance

}

func (cdb *CensoredDb) saveCensoredAuthors(authors []string) {
	cdb.mux.Lock()
	for _, item := range authors {
		cdb.censoredAuthors[item] = item
	}
	cdb.mux.Unlock()

}

func (cdb *CensoredDb) checkCensoredAuthors(author string) bool {
	cdb.mux.RLock()
	for _, item := range cdb.censoredAuthors {
		if author == item {
			return true
		}
	}
	cdb.mux.RUnlock()
	return false
}
func (cdb *CensoredDb) getCensoredAuthors() []string {
	cenArr := make([]string, len(cdb.censoredAuthors))
	cdb.mux.RLock()
	i := 0
	for _, item := range cdb.censoredAuthors {
		cenArr[i] = item
		i++
	}
	cdb.mux.RUnlock()
	return cenArr
}
